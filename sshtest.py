import subprocess

with open(hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        with open("commands.txt") as fh:
            completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p 2222", capture_output=True, text=True, stdin=fh)
            completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p 2223", capture_output=True, text=True, stdin=fh)
            completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p 2224", capture_output=True, text=True, stdin=fh)
